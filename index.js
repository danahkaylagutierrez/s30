const express = require('express')
const mongoose = require('mongoose')

const app = express();

const port = 3001;

//Allows our app to read JSON data
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// 
// app.listen(port, ()=> console.log(`Server running at port ${port}`))


/*

Step 12 : Install mongoose
npm install mongoose

Step 13: import mongoose
const mongoose = require('mongoose')

 STep 14 go to MongoDB and allow access to 0.0.0.0*/

//STEP 15
//get connection string & change Password to your password
// mongodb+srv://admin:admin1234@zuitt-bootcamp.r2a4x.mongodb.net/myFirstDatabase?retryWrites=true&w=majority

//Step 16 Change myFirstDatabse to s30. MongoDB will automatically create the datbse for us
// mongodb+srv://admin:admin1234@zuitt-bootcamp.r2a4x.mongodb.net/s30?retryWrites=true&w=majority

//STEP 17 COnnecting to MongoDB ATlas -add .connect method
//mongoose.connect();

//STEP 18 Add the connection string
/*STEP 18B Add this object to allow connection
	{
	useNewUrlPasser: true,
	useUnifiedTopology: true
	}
*/

mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.r2a4x.mongodb.net/s30?retryWrites=true&w=majority',
	{
	useNewUrlParser: true,
	useUnifiedTopology: true
	}

);

/*STEP 19: Set notification for connection success or failure by using .connection property of mongoose
mongoose.connection;

STEP 20: Store it in variable called db (for database)
*/

let db = mongoose.connection;

/*STEP 21: console.error.bind(console) allows us to print erros  in the browser console and in the terminal*/
db.on("error", console.error.bind(console, "connection error"));

/*STEP 22: if the connection is successful, output this in the console*/
db.once("open",()=> console.log("We're connected to the cloud database"))

/*STEP 23: Schemas determine the structur of the documents to be written in the database
Schemas act as blur prints to our data
Use the schema() constructor of the Mongoose module to create a new Schema object
*/

const taskSchema = new mongoose.Schema({
	/*Define the fields of the corresponding data type
	For a tasl, it needs  "task name" and "task status"
	There is a field called "name" and its data type is "String"*/
	
	name: String,

	//There is a field called "status" that is a "String" and the default value is "pending"
	
	status: {
		type: String,
		default: "pending"
	}
});

/* Models use Schemas and they act as the middleman from the server (JS Code) to our database
STEP 24:  Create a model
   MODELS must be in singular form and capitlized
   -in where to store the data(tasks, plural form of the model)
   -tasks*/

const Task = mongoose.model("Task", taskSchema)

/*
Business logic
1. Add a functionality to check if there are duplicate tasks
  -- if the task already exists in the dtabase, we return an error
   --if the task doesn't exist in the database, we add it in the database
*/

// //Step 25 Create a route to add task
// app.post('/tasks', (req,res)=>{
// 	req.body.name;
// })

/*STEP 26: Check if the task already exists. 
Use the task model to interact with task collection*/

app.post('/tasks', (req,res)=>{
	Task.findOne({name: req.body.name}, (err, result)=>{
		if(result != null && result.name ==req.body.name){
			res.send('Duplicate task found')
		}else{
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save((saveErr, savedTask)=>{
				if(saveErr){
					return console.error(saveErr)
				}else{
					return res.status(201).send('New task added')
				}

			})
		}
	})
})

//STEP 27 get all tasks
//See the contents of the stask collection via Task model
app.get("/tasks", (req, res) => {
		Task.find({},(err, result)=>{
			if(err){
					return console.log(err)
			}else{
				return res.status(200).json({data: result})
			}
		})
})

/*1. Create a User schema

Properties:
username - string
password - string




*/


const userSchema = new mongoose.Schema({
	username: String,
	password: String

});

const User = mongoose.model("User", userSchema)


app.post('/signup', (req,res)=>{
	User.findOne({username: req.body.username}, (err, result)=>{
		if(result != null && result.username ==req.body.username){
			res.send('User already exists')
		}else{
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})

			newUser.save((saveErr, savedTask)=>{
				if(saveErr){
					return console.error(saveErr)
				}else{
					return res.status(201).send('New user added')
				}

			})
		}
	})
})

app.listen(port, ()=> console.log(`Server running at port ${port}`))